// Token dari Akun Mapbox
const token = "pk.eyJ1IjoiZGl2YWxnIiwiYSI6ImNqc256MnFmMTA4dG00NHA1dnBjd2dxZnoifQ.XnHQB4Xy2y3RkpaNJ-Bm3w";

// Default params onload Map
const container = 'map';
const style = 'mapbox://styles/mapbox/light-v10';
const zoom = 18;
const center = [124.8489077, 1.4840919];
const pitch = 60;
const antialias = true;

// List Model
const model3d = [
    { "latlng": [124.8499946, 1.4843343], "rotate_x": 1, "rotate_y": 2, "rotate_z": 1, "altitude": 0, "src": "file/1.gltf" },
    { "latlng": [124.8489077, 1.4840919], "rotate_x": 2, "rotate_y": 0.5, "rotate_z": 2, "altitude": 20, "src": "file/1.gltf" },
    { "latlng": [124.8479946, 1.4845343], "rotate_x": 3, "rotate_y": 3.5, "rotate_z": 3, "altitude": 40, "src": "file/1.gltf" },
    { "latlng": [124.8469946, 1.4837343], "rotate_x": 4, "rotate_y": 1.5, "rotate_z": 4, "altitude": 60, "src": "file/1.gltf" },
];
